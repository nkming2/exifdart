import 'dart:io';

import 'package:exifdart/src/blob_reader_io.dart';
import 'package:exifdart/src/blob_view.dart';
import 'package:exifdart/src/heic_metadata_finder.dart';
import 'package:exifdart/src/log_message_sink.dart';
import 'package:test/test.dart';

void main() {
  test("Test HeicMetadataFinder exif (no exif)", () async {
    final file = File("test/1.heic");
    final dataView = await BlobView.create(FileReader(file));
    return HeicMetadataFinder(_Log())(dataView)
        .then((result) => expect(result.exifExtentOffset, null));
  });

  test("Test HeicMetadataFinder exif (iloc v0)", () async {
    final file = File("test/1-2.heic");
    final dataView = await BlobView.create(FileReader(file));
    return HeicMetadataFinder(_Log())(dataView)
        .then((result) => expect(result.exifExtentOffset, 0x029C));
  });

  test("Test HeicMetadataFinder exif (iloc v1)", () async {
    final file = File("test/2.heic");
    final dataView = await BlobView.create(FileReader(file));
    return HeicMetadataFinder(_Log())(dataView)
        .then((result) => expect(result.exifExtentOffset, 0x1FD429));
  });

  test("Test HeicMetadataFinder exif (exif partially removed)", () async {
    final file = File("test/3-2.hif");
    final dataView = await BlobView.create(FileReader(file));
    return HeicMetadataFinder(_Log())(dataView)
        .then((result) => expect(result.exifExtentOffset, null));
  });

  test("Test HeicMetadataFinder size (non-derived)", () async {
    final file = File("test/1.heic");
    final dataView = await BlobView.create(FileReader(file));
    return HeicMetadataFinder(_Log())(dataView).then((result) =>
        expect([result.imageWidth, result.imageHeight], [1440, 960]));
  });

  test("Test HeicMetadataFinder size (derived: image grid)", () async {
    final file = File("test/2.heic");
    final dataView = await BlobView.create(FileReader(file));
    return HeicMetadataFinder(_Log())(dataView).then((result) =>
        expect([result.imageWidth, result.imageHeight], [4032, 3024]));
  });

  test("Test HeicMetadataFinder size (rotated)", () async {
    final file = File("test/4.heic");
    final dataView = await BlobView.create(FileReader(file));
    // stored as 5472*3648. displayed as 3648*5472 after rotated 90deg
    return HeicMetadataFinder(_Log())(dataView).then((result) =>
        expect([result.imageWidth, result.imageHeight], [5472, 3648]));
  });

  test("Test HeicMetadataFinder orientation (no rotation property)", () async {
    final file = File("test/1.heic");
    final dataView = await BlobView.create(FileReader(file));
    return HeicMetadataFinder(_Log())(dataView)
        .then((result) => expect(result.rotateAngleCcw, null));
  });

  test("Test HeicMetadataFinder orientation (0 deg)", () async {
    final file = File("test/3.hif");
    final dataView = await BlobView.create(FileReader(file));
    return HeicMetadataFinder(_Log())(dataView)
        .then((result) => expect(result.rotateAngleCcw, 0));
  });

  test("Test HeicMetadataFinder orientation (90 deg)", () async {
    final file = File("test/4.heic");
    final dataView = await BlobView.create(FileReader(file));
    return HeicMetadataFinder(_Log())(dataView)
        .then((result) => expect(result.rotateAngleCcw, 90));
  });

  test("Test HeicMetadataFinder (not HEIC)", () async {
    final file = File("test/5.jpg");
    final dataView = await BlobView.create(FileReader(file));
    return expect(HeicMetadataFinder(_Log())(dataView), throwsException);
  });
}

class _Log extends LogMessageSink {
  @override
  void log(Object? message, [List<Object>? additional]) {
    // Uncomment to show logs
    // print("$message");
  }
}
