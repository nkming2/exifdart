import "dart:async";
import "dart:typed_data";

import "abstract_reader.dart";
import "exif_extractor.dart";

/// Reads the EXIF info from an image already loaded into memory
Future<Map<String, dynamic>?> readExifFromBytes(Uint8List bytes) {
  return readExif(new MemoryBlobReader(bytes));
}

/// Uses a byte buffer to read the blob data from.
class MemoryBlobReader extends AbstractBlobReader {
  /// Use this constructor to create a memory blob reader with the given list of bytes.
  /// The [bytes] aren't copied if the type is `Uint8List`.
  MemoryBlobReader(List<int> bytes)
      : _bytes = bytes is Uint8List ? bytes : new Uint8List.fromList(bytes);

  @override
  int get byteLength => _bytes.lengthInBytes;

  @override
  ByteData readSlice(int start, int end) {
    if (start >= _bytes.lengthInBytes) {
      return _bytes.buffer.asByteData(_bytes.offsetInBytes, 0);
    }
    if (end > _bytes.lengthInBytes) end = _bytes.lengthInBytes;
    return _bytes.buffer.asByteData(start + _bytes.offsetInBytes, end - start);
  }

  final Uint8List _bytes;
}
